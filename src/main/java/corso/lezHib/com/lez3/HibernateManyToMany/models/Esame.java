package corso.lezHib.com.lez3.HibernateManyToMany.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "esame")
public class Esame {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "esameID")
	private int id;
	
	@Column(name = "titolo")
	private String titolo;
	@Column(name = "crediti")
	private String crediti;
	@Column(name = "data_esame")
	private String dataEsa;
	
	@ManyToMany
	@JoinTable(name = "iscrizione_studente_esame",
			joinColumns = { @JoinColumn(name="esameRif", referencedColumnName = "esameID") },
			inverseJoinColumns = { @JoinColumn(name="studenteRif", referencedColumnName = "studenteID")}
	)
	private List<Studente> studenti;
	
}
