package corso.lezHib.com.lez3.HibernateManyToMany.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "studente")
public class Studente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "studenteID")
	private int id;
	
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column
	private String matricola;
	
	@ManyToMany
	@JoinTable(name = "iscrizione_studente_esame",
			joinColumns = { @JoinColumn(name="studenteRif", referencedColumnName = "studenteID")},
			inverseJoinColumns = { @JoinColumn(name="esameRif", referencedColumnName = "esameID") })
	private List<Esame> esami;
	
}









